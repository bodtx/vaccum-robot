// This is the library for the TB6612 that contains the class Motor and all the
// functions
#include <Timemark.h>
#include <L298N.h>

//pin definition
#define ENA 9
#define ENB 10
#define INA1 5
#define INA2 6
#define INB1 12
#define INB2 13
#define FAN 4
#define BROSSE 3
#define HALL 2

//create a motor1 instance
L298N motor1(ENA, INA1, INA2);
L298N motor2(ENB, INB1, INB2);
Timemark cleaningDelay(1200000);
Timemark forwardDelay(6000);
Timemark rpmDelay(50);

volatile bool isBrushing;
byte countL;
byte countR;



void count_function()
{
  if (!isBrushing) {
    isBrushing = true;
  }

}


void setup()
{

  pinMode(A0, INPUT_PULLUP);
  pinMode(A1, INPUT_PULLUP);
  pinMode(FAN, OUTPUT);
  pinMode(BROSSE, OUTPUT);
  pinMode(HALL, INPUT_PULLUP); //Sets sensor as input

  digitalWrite(FAN, HIGH);
  //brosse
  rpmDelay.start();
  attachInterrupt(digitalPinToInterrupt(HALL), count_function, RISING); //Interrupts are called on Rise of Input
  isBrushing = false;
  delay(10000);
  analogWrite(BROSSE, 50);
  delay(1500);
  analogWrite(BROSSE, 100);
  delay(1500);
  analogWrite(BROSSE, 180);

  forwardDelay.start();
  cleaningDelay.start();//30 minutes de nettoyage




}


void loop()
{

  if (!cleaningDelay.expired() && cleaningDelay.running()) {
    if (rpmDelay.expired()) {
      if (!isBrushing) {
        shutdown();
      }
      else {
        isBrushing = false;
        rpmDelay.start();
      }
    }


    forward(motor1, motor2, 150);
    //on avance un certain temps car si on est coincé sans que les capteurs aient détecté de choc, on va patiner dans le vide sans fin.
    if (forwardDelay.expired()) {
      if (random(0, 2) == 0)
        dodge('L');
      else
        dodge('R');
    }
    if (digitalRead(A0) == LOW) {
      if (countL < 5) {
        dodge('L');
        countL++;
        countR = 0;
      }
      else dodge('R');
    } else if (digitalRead(A1) == LOW) {
      if (countR < 5) {
        dodge('R');
        countR++;
        countL = 0;
      }
      else dodge('L');
    }
  }
  else {
    shutdown();
  }

}

void dodge(char direction) {
  forward(motor1, motor2, 80);
  delay(200);
  brake(motor1, motor2);
  delay(100);
  back(motor1, motor2, 100);
  delay(500);
  switch (direction) {
    case 'L':
      left(motor1, motor2, 100);
      break;
    case 'R':
      right(motor1, motor2, 100);
      break;
  }
  delay(random(200, 1500));
  brake(motor1, motor2);
  delay(100);
  forward(motor1, motor2, 100);
  delay(500);
  forwardDelay.start();


}

void shutdown() {
  digitalWrite(BROSSE, LOW);
  digitalWrite(FAN, LOW);
  brake(motor1, motor2); //fin du cycle de nettoyage
  forwardDelay.stop();
  cleaningDelay.stop();
}

void forward(L298N motor1, L298N motor2, int speed) {
  motor1.setSpeed(speed);
  motor2.setSpeed(speed);
  motor1.forward();
  motor2.forward();
}

void left(L298N motor1, L298N motor2, int speed) {
  motor1.setSpeed(speed);
  motor2.setSpeed(speed);
  motor1.forward();
  motor2.backward();
}

void right(L298N motor1, L298N motor2, int speed) {
  motor1.setSpeed(speed);
  motor2.setSpeed(speed);
  motor2.forward();
  motor1.backward();
}

void back(L298N motor1, L298N motor2, int speed) {
  motor1.setSpeed(speed);
  motor2.setSpeed(speed);
  motor2.backward();
  motor1.backward();
}

void brake(L298N motor1, L298N motor2) {
  motor2.stop();
  motor1.stop();
}
